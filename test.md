---
  title: Test
  template: /home/irvin/Templates/pandoc.tex
  bibliography: library.json
  csl: itw.csl
---

# Wiederholungen

Erstzitierung eindeutig.[@mustermannWiederholung2008] _Mustermann, Herbert: Die Wiederholung. Bern 2008._

Sofortige Wiederholung eindeutig.[@mustermannWiederholung2008] _Mustermann 2009._

Sofortige Wiederholung mit Seitenzahlen.[@mustermannWiederholung2008 S. 12-15] _Mustermann 2009, S. 12-15._

Erstzitierung uneindeutig 1.[@mullerFruhlingsgeschichte2010] _Müller: Eine Frühlingsgeschichte 2010._

Erstzitierung uneindeutig 2.[@mullerHerbstgeschichte2010] _Müller, Diana: Eine Herbstgeschichte. Zürich 2010._

Sofortige Wiederholung uneindeutig mit Seitenzahl 2.[@mullerHerbstgeschichte2010 S. 12] _Müller: Eine Herbstgeschichte 2010, S. 12._

Wiederholung uneindeutig mit Seitenzahl 1.[@mullerFruhlingsgeschichte2010 S. 12] _Müller: Eine Herbstgeschichte 2010, S. 12._

(Für Test.[@fischer-lichteTheaterwissenschaft:2009] _Fischer-Lichte, E.: Theaterwissenschaft: Eine Einführung in die Grundlagen des Fachs. 2009_)

Nochmalige Wiederholung.[@mustermannWiederholung2008] _Mustermann 2009._

Nochmalige Wiederholung Erstzitierung uneindeutig 1.[@mullerFruhlingsgeschichte2010] _Müller: Eine Frühlingsgeschichte 2010._

Nochmalige Wiederholung Erstzitierung uneindeutig mit Seitenzahl 2.[@mullerFruhlingsgeschichte2010 S. 12] _Müller: Eine Herbstgeschichte 2010, S. 12._

# Selbstständige Publikationen
## Monografien
Monografie.[@kotteTheaterwissenschaft2012 S. 12] _Kotte, Andreas: Theaterwissenschaft. Eine Einführung. Köln, Weimar u. Wien 2012, S. 12._

Monografie mit zwei Autoren.[@name1MonografieAuthors2000]

Monografie mit drei Autoren.[@name1MonografieAuthors2000a]

Monografie mit vier Autoren.[@name1MonografieAuthors2000b]

Monografie mit drei Orten.[@name1MonografiePlaces2000a]

## Sammelbände
Sammelband.[@editorname1Sammelband2010]

Sammelband mit zwei Herausgebern.[@editorname1SammelbandEditors2000]

Sammelband mit drei Herausgebern, unsortiert.[@christenMonografieAuthorsUnordered2000]

# Unselbständige Publikationen

Beitrag in Sammelband. [@name1BeitragSammelband2000a]

Beitrag in Sammelband, zwei Herausgeber [@name1BeitragSammelbandZwei2000]

Beitrag in Sammelband, drei Herausgeber [@name1BeitragSammelbandDrei2000]


# Lustiges

## Theateraufführung

Eine Theateraufführung[@shakespeareHamlet2001]


## Film

Ein Film[@vontrierDogville2003]

# Bibliografie
