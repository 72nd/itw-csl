# ITW CSL

## Übersicht

Die [CSL](https://de.wikipedia.org/wiki/Citation_Style_Language) ermöglicht es, Zitierungen entsprechend den [Richtlinien zum Verfassen wissenschaftlicher Arbeiten](https://www.theaterwissenschaft.unibe.ch/e266971/e669210/e669240/RichtlinienzumVerfassenwissenschaftlicherArbeiten_ger.pdf) des [Instituts für Theaterwissenschaft Bern](https://www.theaterwissenschaft.unibe.ch/) automatisch generieren zu lassen. Hierfür ist die Verwendung einer Literaturverwaltung notwendig. Empfehlenswert ist [Zotero](https://www.zotero.org/), eine freies und offenes Programm ([deutsche Dokumentation](https://www.zotero.org/support/de/start)).

## Hinweise zur Benutzung

Das Stylesheet [hier](https://gitlab.com/72th/itw-csl/raw/master/itw.csl) mit Rechtsklick auf den Link und «speichern unter» herunterladen und dem Literaturprogramm einkonfigurieren.

### Allgemein

- Autor.innen sollten in Zotero korrekt sortiert eingegeben werden.
- Die Trennung von Titel und Untertitel mit einem Punkt muss beim Eintragen in die Literaturverwaltung manuell vorgenommen werden.
- Die Hinweise auf dieser Seite, welche sich auf JSON beziehen, können ignoriert werden.

### Theateraufführungen

Für Theateraufführungen gibt es kein eigener Item Type, es wird statt dessen der Type «Hearing» (im JSON als «bill» refferenziert) benutzt. Die Felder haben folgende Entsprechung:

| ITW                       | Zotero           | JSON        |
|---------------------------|------------------|-------------|
| Titel der Inseznierung    | Title            | title       |
| «von/nach» Autor.in       | Extra            | note        |
| Autor.in                  | Contributor      | author      |
| «Regie/Choreographie»     | Committee        | section     |
| Regisseur.in              | Publisher        | publisher   |
| Theater, Aufführungsort   | Place            | event-place |
| Ggf. Aufführungsort       | Legislative Body | authority   |
| Datum Premiere            | Date             | issued      |
| Anmerkung zu Datum Besuch | Session          | event-place |
| Datum Besuch              | Accessed         | accessed    |



### Film

In Zotero «Film» auswählen (JSON: «motion_picture»).

| ITW               | Zotero       | JSON       |
|-------------------|--------------|------------|
| Titel             | Title        | title      |
| Regisseur.in      | Director     | author     |
| Produktionsländer | Extra        | note       |
| Jahr              | Date         | issued     |
| Dauer in Minuten  | Running Time | dimensions |


## Stand

Der Style unterstützt die meisten Anwendungsfälle. Eine gute Überblick bietet die [Test Datei](test.pdf). Fehlerhafte Fälle begründen sich meist dadurch, dass ich in der Praxis selbst noch keinen Anwendungsfall hatte. Rückmeldungen sind also willkommen. Bekannte Problemfälle sind folgende:

- Spaltenzahlen bei Artikel in Lexika

## Changelog

- 7. Oktober 2019: Zitierung von Filmen funktioniert nun richtig.
- 18. November 2019: Das die Datumsangabe des Aufführungsbesuches kann um einen Kommentar ergänzt werden.
- 18. November 2019: Reihenpublikationen werden nun mit Titel und Bandnummer angegeben.

## Autorenschaft und Lizenz

Des ITW CSL basiert auf dem [infoclio.ch](https://editor.citationstyles.org/styleInfo/?styleId=http%3A%2F%2Fwww.zotero.org%2Fstyles%2Finfoclio-de) Style in der Version vom 27. März 2018 und ist ebenfalls unter der Creative Commons Lizenz [Namensnennung - Weitergabe untergleichen Bedingungen 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.de) lizenziert. Autoren des infoclio-de Style:

- Enrico Natale
- Jan Baumann
- Jonas Schneider


